//
//  ViewController.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 26/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    
    @IBOutlet var productosTable: UITableView!
    @IBOutlet var barraBusqueda: UISearchBar!
    @IBOutlet var busquedasTable: UITableView!
    
    var listaDeProductos = [Producto]()
    var listaDeImagenesProductos = [UIImage]()
    var requestsControl: RequestManager? = nil
    var listaBusquedas = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if UserDefaults.standard.object(forKey: "listaBusquedas") != nil {
            listaBusquedas = UserDefaults.standard.object(forKey: "listaBusquedas") as! [String]
        }
        
        
        busquedasTable.isHidden = true
        busquedasTable.tag = 1
        busquedasTable.delegate = self
        busquedasTable.dataSource = self
        
        
        productosTable.tag = 2
        productosTable.delegate = self
        productosTable.dataSource = self
        
        barraBusqueda.delegate = self
        
        requestsControl = RequestManager(parentCtrl: self)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UISearchBar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        busquedasTable.isHidden = false
        busquedasTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        requestsControl?.callProductListService(productName: searchBar.text!)
        searchBar.endEditing(true)
        
        busquedasTable.isHidden = true
        
        listaBusquedas.append(searchBar.text!)
        
        listaBusquedas = Array(Set(listaBusquedas))
        
        UserDefaults.standard.set(listaBusquedas, forKey: "listaBusquedas")
    }
    
    
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return listaBusquedas.count
        }
        else {
            return listaDeProductos.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "busquedaCell") as! BusquedasAnterioresTableViewCell
            
            cell.nombre.text = listaBusquedas[indexPath.row]
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! ProductoTableViewCell
            
            let producto = listaDeProductos[indexPath.row]
            
            cell.nombre.text = producto.nombre
            cell.precio.text = String(producto.precio).currencyFormatFromDoubleValue()
            cell.imagen.image = listaDeImagenesProductos[indexPath.row]
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            let cell = tableView.cellForRow(at: indexPath) as! BusquedasAnterioresTableViewCell
            
            self.barraBusqueda.text = cell.nombre.text
            self.searchBarSearchButtonClicked(barraBusqueda)
        }
    }

}

