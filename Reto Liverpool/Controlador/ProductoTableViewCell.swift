//
//  ProductoTableViewCell.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 27/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import UIKit

class ProductoTableViewCell: UITableViewCell {
    
    @IBOutlet var nombre: UILabel!
    @IBOutlet var precio: UILabel!
    @IBOutlet var imagen: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
