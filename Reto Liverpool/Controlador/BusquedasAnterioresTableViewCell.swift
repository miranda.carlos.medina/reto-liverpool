//
//  BusquedasAnterioresTableViewCell.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 27/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import UIKit

class BusquedasAnterioresTableViewCell: UITableViewCell {

    @IBOutlet var nombre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
