//
//  WSProductListResponse.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 27/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import Foundation

struct WSProductListResponse: Codable {
    var plpResults = PLPResults()
}


struct PLPResults: Codable {
    var records = [Records]()
}

struct Records: Codable {
 
    var productDisplayName = ""
    var listPrice = 0.0
    var smImage = ""
}

