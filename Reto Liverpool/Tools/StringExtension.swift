//
//  StringExtension.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 27/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import Foundation

extension String {
    
    func currencyFormatFromDoubleValue() -> String {
        let formatter = NumberFormatter()
        
        let amount = self
        
        formatter.locale = Locale.current 
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double(amount)! as NSNumber) {
            return "\(formattedTipAmount)"
        }
        return "☠️"
    }
}
