//
//  CodableDecoder.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 27/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import Foundation

open class CodableDecoder{
    
    static func DecodeJsonData<T:Codable>(data : Data)->T?{
        let decoder = JSONDecoder()
        do {
            let responseObject = try decoder.decode( T.self , from: data)
            return responseObject
        } catch {
            // print(error)
        }
        return nil
    }
    
    static func EncodeObjectToJSON<T:Codable>( object : T )->[String: Any]?{
        
        do {
            let encodedData = try JSONEncoder().encode(object)
            let dictionary = try JSONSerialization.jsonObject(with: encodedData, options: .allowFragments) as? [String: Any]
            return dictionary
        } catch {
            // print(error)
        }
        return nil
    }
    
    static func EncodeObjectToGenericJSON<T:Codable>( object : T )->Data?{
        
        do {
            let encodedData = try JSONEncoder().encode(object)
            return encodedData
        } catch {
            // print(error)
        }
        return nil
    }
}
