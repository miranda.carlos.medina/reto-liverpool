//
//  RequestManager.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 26/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import Foundation
import UIKit



class RequestManager {
    
    var parentCtrl : ViewController
    
    
    init(parentCtrl : ViewController){
        self.parentCtrl = parentCtrl
    }
    
    public func callProductListService(productName: String) {
        
        let urlString = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v2/plp?search-string=" + productName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            do {
                
                let result: WSProductListResponse? = CodableDecoder.DecodeJsonData(data: data)
                
                if result != nil {
                    
                    if result!.plpResults.records.count > 0 {
                        var listaDeProductos = [Producto]()
                        
                        
                        self.parentCtrl.listaDeImagenesProductos = []
                        
                        for _ in result!.plpResults.records {
                            self.parentCtrl.listaDeImagenesProductos.append(UIImage.init())
                        }
                        
                        
                        for (i, record) in ((result?.plpResults.records)?.enumerated())! {
                            
                            let producto = Producto.init(nombre: record.productDisplayName, precio: record.listPrice, imagenURL: record.smImage)
                            
                            self.callProductImagesListService(urlString: record.smImage, producto: producto, index: i)
                            
                            listaDeProductos.append(producto)
                        }
                        
                        self.parentCtrl.listaDeProductos = listaDeProductos
                        
                    }
                    else { self.mostrarAviso() }
                }
                else { self.mostrarAviso() }
                
                
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
        
    }
    
    public func callProductImagesListService(urlString: String, producto: Producto, index: Int) {
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            do {
                
                if let image = UIImage(data: data) {
                    
                    self.parentCtrl.listaDeImagenesProductos[index] = image
                    DispatchQueue.main.async {
                        producto.imagen = image
                        if self.parentCtrl.listaDeImagenesProductos.count == self.parentCtrl.listaDeProductos.count {
                            self.parentCtrl.productosTable.reloadData()
                        }
                        else {
                            print("👽")
                        }
                        
                    }
                }
                
                
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
        
    }
    
    func mostrarAviso() {
        let alert = UIAlertController(title: "Aviso", message: "Sin resultados", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }
            self.parentCtrl.barraBusqueda.text = ""
            self.parentCtrl.listaDeProductos = []
            
            DispatchQueue.main.async {
                self.parentCtrl.productosTable.reloadData()
            }
            
        }))
        self.parentCtrl.present(alert, animated: true, completion: nil)
    }
    
}
