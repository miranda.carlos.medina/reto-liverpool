//
//  Producto.swift
//  Reto Liverpool
//
//  Created by Carlos Miranda Medina on 26/09/18.
//  Copyright © 2018 Carlos Miranda Medina. All rights reserved.
//

import UIKit

class Producto: NSObject {

    var nombre: String?
    var precio = 0.0
    var imagen: UIImage?
    var imagenURL: String?
    
    init(nombre: String, precio: Double, imagenURL: String) {
        self.nombre = nombre
        self.precio = precio
        self.imagenURL = imagenURL
    }
}
